---
title: "Giving the finger (back) to the digital: considering \"visual vocabularies\" in relation to the photographic archive of Asger Jorn's SICV"
author: "Scandinavian Institute for Computational Vandalism (Nicolas Malevé, Michael Murtaugh, Ellef Prestsæter)"
---

> "The book is well written, and it shows talent. But please note that I'm using a word, which is more suited to describing a work of art than a scientific work. It seems to me you are an artist, an excellent artist from what I can gather from your book, but are you a scientist? [...] The strengths and weaknesses of your book lie precisely in the fact that you are unaware of (or dismiss too easily) scientific method".  
Letter from Prof. Bent Schultzer to Asger Jorn, December 20, 1952.[^baumeister]

[^baumeister]: Ruth Baumeister, *Asger Jorn in Images, Words and Forms* [Scheidegger 2014], p. 46; The quotation is from a letter written in response to Jorn's submission of his text *Luck and Chance* to the Faculty of Philosophy at the University of Copenhagen.

> We will have a descriptor for each interest point. And we're in high dimensional space, and we want to then cluster them to come up with the words and that will be our vocabulary. [The] English language has a vocabulary for thousands of years, but [for] visual words we don't have a vocabulary, so we have to come up with our own...  
Prof. Mubarak Shah, addressing students of a University level engineering course titled *Computer Vision*, 2012[^ucf]

[^ucf]: Mubarak Shah, "Lecture 17: Bag-of-Features (Bag-of-Words)" *UCF Computer Vision Video Lectures 2012* [University of Central Florida, 2012], [https://youtu.be/iGZpJZhqEME?t=15m59s](https://youtu.be/iGZpJZhqEME?t=15m55s) Accessed May 2017 More information on the course can be found at [http://crcv.ucf.edu/courses/CAP5415/Fall2012/](http://crcv.ucf.edu/courses/CAP5415/Fall2012/)


"Black" culture
-----------------
Artyom opens his web browser and types in "google.com". He is redirected to google.nl and the interface appears in Dutch. At the top corner of the screen he sees a user name and an image of his "avatar" related to his google account. He selects "log out" and clicks "Google.com gebruiken" and the interface changes into English. He clicks "Images" at the top right, and begins to type the word "culture" in the search box; as he types each letter, a stream of suggestions appears below his own text. C: cat, cricket score, cars, calendar 2016, couple sleeping positions and what they mean. CU: cute puppies, cute quotes, cute animals, cute baby, cuba. CUL: culottes, culture, culture club, cult, cul de sac. He clicks "culture" and the page reloads with a grid of images.

![](originals/googleimagesearch_01_culture.png)

At the top of the results is a special row of images appearing together with texts below them: "Diversity", "The Word", "Different", "World", "American", and "Traits". Below this eleven images are shown in two rows underneath which further results lie in wait of scrolling. Of the seventeen images on the screen, six have the word "culture" legible within them. Five of the images contain a circular depiction of the globe showing North and South America and a slice of the African continent. In each of these images, the globe is surrounded by symbols representing people in a rainbow palette of colors. Six of the images contain multiple images of national flags. Nearly all the images appear on a white background.

Above the grid is a row of clickable links indicating different modes of search: *All*, *Images* (the mode now active), *News*, *Books*, *Videos*, *More*, and *Search tools*. Clicking on *Search Tools* reveals a second row of options specific to images: *Size*, *Color*, *Type*, *Time*, *Usage rights*, and *More tools*. Clicking *Type* reveals sub-items like *Photo*, *Clip art*, and *Line drawing*.  Clicking on *Color* opens a selection panel with options *Any color* (active), *Full color*, *Black and white*, *Transparent* and a grid of twelve color swatches (red, orange, yellow, green, etc.). He selects the swatch depicting the color black. The page reloads with twenty image results visible.

![](originals/googleimagesearch_02_blackculture1.png)

The first result is the only to contain the word "culture", it appears to be the thumbnail of a poster with a grid of even smaller images and the text "November 3rd-8th Black Culture Awareness week 2013". Four of the images contain visibly the word "black" (two of which are images of publications: Negro Digest, a 1969 magazine cover with the text "Black World", and the cover of a book titled "Black Noise: Rap Music and Black Culture in Contemporary America" by Tricia Rose). Seven of the images are black and white. Many are images of well-known African American musicians: the rapper Red Pill standing before a wall painting of the musician Biggie Smalls, Tupac Shakur, KRS-One, Thelonious Monk, as well as people associated to historical civil rights movements: Amiri Baraka, Malcolm X, Zora Neale Hurston. While the overall coloration of the presented images does appear to contain more black pixels than the previous set of results, the results shown clearly seem to be more the result of the adjacency of the textual content "black culture" to the pictures than to the purely chromatic content of the image per se, as the operation of the color swatch filter might suggest.

![](originals/googleimagesearch_02_blackculture2.png)

Repeating the search (images with the text "culture" filtered by the black color swatch) via the Dutch language interface of "google.be" produces completely different results. The first fifteen results include a graphic with the text "Culture is something that unites people. -- Anastasya O., Russia", an image of Miley Cyrus "twerking" in concert, the logo of "Common Culture" depicting the black silhouette of a crown, Shia Laboef wearing a paper bag with the text "I am not famous anymore", a silhouette globe sprouting from a tree, black Japanese kanji characters against a white background, a vividly colored geisha against a black background. In none of the results does the word "black" or "zwart" appear. In contrast to google.com results, the textuality of "black" here seems to be less of influence than its chromatic pixel value.[^artyom]

[^artyom]: This example was suggested in exchanges with the Artyom Kochyarn, an artist who has described to the authors how Google Image Search works as "his camera", the extended search tools a kind of equivalent to the camera's aperture and speed controls. [http://artyomkocharyan.com/](http://artyomkocharyan.com/)

"Green" tongues
---------------

*La Langue verte et la cuite* is a large format photo-book produced by Danish artist Asger Jorn and French writer Noël Arnaud in 1968. The book riffs off the influential 1964 *Le Cru et le cuit*, anthropologist Lévi-Strauss's first in a series on "mythologies" comparing and contrasting recurring elements across different cultures via conceptual dualisms such as "raw" versus "cooked" (hence the title).  "La langue verte et la cuite" is multiple word play: "la langue verte", besides meaning literally a "green tongue", also means popular language or slang. "La cuite" spins the fact that in French, to be "cooked" is slang for being drunk, thus Lévi-Strauss' "The Raw and the Cooked"[^rawandthecooked] becomes "Slang and Drunkenness"[^harris_p112]. Lévi-Strauss's work is decidedly academic, its 400-some pages primarily text, punctuated by a number of diagrams often in the form of mathematical formulas, illustrating schematic relations of concepts typically in binaries such as life/death, father/son, and earth/sky[^levistrauss_figure].

[^rawandthecooked]: *The Raw and the cooked* is the title of the English translation of *Le Cru et le cuit*, by John and Doreen Weightman [Chicago 1969]. This translation is itself incomplete as "cuit" might be better translated as "prepared" as opposed to  strictly "cooked".

*Le Cru et le cuit* has just four photographs appearing in the main text, including a single portrait of a Bororo man, the culture that plays a central role in the work. Besides this, the book contains a special appendix, entitled the "Bestiaire"[^levistrauss_bestiaire], containing a supplemental sixteen pages of engravings of animals referenced by the myths found in the main text.

![© Donation Jorn, Silkeborg.](originals/TONGUESP46.png)

In contrast, *La Langue verte* is driven by images. The vast majority of its pages contain black and white photographs, many printed full bleed, each of which have been overpainted by Jorn with a single color to indicate their "tongues". Some of the tongues would be evident even without the color, but many are rather more the suggestion of the overpainting, a playful gesture following the flourish of a decorative element or activating the negative space within an image. The color of the overpainting changes over the course of the book in accordance with a range of themes, themselves often involving puns and wordplay. The book draws on the photographic archive of the SICV, a project Jorn had started some years before.

> Upon resigning from the Situationist International in 1961, [Jorn] conceived the multi-volume series 10,0000 Years of Nordic Folk Art, and founded the Scandinavian Institute of Comparative Vandalism with a few friends to oversee this project; he is named as director of this institute on the title page of *La Lange verte*. Each volume was to be dedicated to a particular aspect or region of Scandinavian folk culture, with a written section contributed by a scholar and a larger section of photographs to follow. Jorn was struck by the possibilities of André Malraux's various series of books on art, published from 1947 on, and even hired Gérard Franceschi, the chief photographer at the Louvre -- whose photographs Malraux frequently relied on -- to take the majority of the photographs for his own, even larger series.[^harris_pp112-115]

In Steven Harris's reading of *La Lange verte*, he cites an earlier title of the project as a critique to the *disembodied* view of language presented by Levi-Straus:

> Jorn's first title, *La Langue crue et la cuite*, makes explicit the role of the tongue in both eating and speaking, and refers implicitly to the Saussurean linguistic theory underpinning structuralism, which in distinguishing between *la langue* (language) and *la parole* (speech) strips the word *langue* of any association with the body, despite the fact that *langue* denotes both "language" and "tongue".[^harris_pp112-115]

In a note Harris points out how Jorn's own copy of *Le cru et le cruit*, now in the Jorn archive, was heavily annotated, and included the comment "idiot" next to an observation by Lévi-Strauss of painting's "essentially mimetic" function.[^harris_note8]

> Between painting and music, there is no real parity: [painting] finds its material in nature: colors are given before being used; and vocabulary testifies to their derived nature in the designation of more subtle shades: dark blue, peacock blue or blue-petroleum; water green, jade-green; straw-yellow, lemon yellow; cherry-red, etc. In other words, there only exist colors because there are already colored beings and objects, and it is only by abstraction that colors can be detached from these natural substrates and treated as the terms of a separate system.[^levistrauss_colors]

As Harris notes, "Jorn insisted on the separation of text and image, so that images could function independently of text and be arranged according to an order intrinsic to the images themselves".[^harris_pp112-115] The plans for an extensive series of publications from the SICV ultimately failed to find financial backing, in part due to Jorn's insistence on having editorial control to make the particular kind of visually driven publications he wished to make. When the SICV was still active (between 1961 and 1965), two books were produced using Franceschi's photographs, a pilot volume for the 10,000 of Nordic Folk Art series on *12th Century Scanian Stone Sculpture*, and *Signes gravés sur les églises de l'Eure et du Calvados*, a photo-book of graffiti carvings found in churches in Normandy. Beyond exhibiting simply a separation of text and image, a key feature in each of the books was the *primacy* of the image, with high quality full bleed photographs printed without captions. Each image is discretely labeled with a numerical reference which leads the interested reader to a corresponding caption found in appendix form later in the book. Jorn thus reversed the relationship of text to image typical of academic writing. In contrast to the reductive black and white engravings of animals found in Lévi-Strauss's *Bestiaire*, where each species is reduced to a single representative example, Franceschi's photographs rewrite the conventions of "objective" photographic representation of archaeological objects, employing dramatic lighting rather than flat, accentuating shadows and expressive details rather than repressing them.[^Teresa] Jorn's presentation of these "portraits of stones"[^fuller] is reflected in his efforts to print the books with multiple layers of ink to enhance the dynamic range of the blackness of the images[^henriksen_p227].

[^harris_p112]: Steven Harris, "How Language Looks: On Asger Jorn and Noël Arnaud's La Langue verte", October 141 [2012], p. 112

[^harris_note8]: Harris, p. 114, note 8.

[^harris_pp112-115]: Harris, pp. 112-115

[^Teresa]: Teresa Østergaard Pedersen has intriguingly developed the archaeological implications of this visual strategy in her "The Image as Agent: 'Comparative Vandalism' as Visual Strategy", [http://sicv.activearchives.org/w/The_Image_as_Agent](http://sicv.activearchives.org/w/The_Image_as_Agent).

[^fuller]: Matthew Fuller, [http://sicv.activearchives.org/w/Computational_Vandalism](http://sicv.activearchives.org/w/Computational_Vandalism)

[^henriksen_p227]: Niels Henriksen, "Vandalist Revival: Asger Jorn's Archaeology", *Asger Jorn: Restless Rebel* [Prestel 2014] p.227

[^levistrauss_colors]: Claude Lévi-Strauss, *Le Cru et le cuit*, [Plon 1964], pp. 27-28. Author's translation of original.

	> Entre peinture et musique; il n'existe donc pas de parité véritable. L'une trouve dans la nature sa matière: les couleurs sont données avant d'être utilisées; et le vocabulaire atteste leur caractère dérivé jusque dans la désignation des plus subtiles nuances: bleu-nuit, bleu-paon ou bleu-pétrole; vert d'eau, vert-jade; jaune-paille, jaune citron; rouge-cerise, etc. Autrement dit, il n'existe de couleurs en peinture que parce qu'il y a déjà des êtres et des objets colorés, et c'est seulment par abstraction que les couleurs peuvent être décollées de ces substats naturels et traitées comme les termes d'un système séparé.

[^levistrauss_figure]: Claude Lévi-Strauss, *Le Cru et le cuit*, [Plon 1964], p. 70

[^levistrauss_bestiaire]: Claude Lévi-Strauss, *Le Cru et le cuit*, [Plon 1964], pp. 349-366

[^holm]: "Poking Tongues: On Asger Jorn's Intellectual Endeavours as governed by artistic experience", *Asger Jorn: Restless Rebel* [Prestel 2014] pp 238-249


Tracings
------------------------

In 2014, the authors initiated a project to investigate what it might mean to apply digital tools and algorithms to the photographic archive of the SICV.[^activearchives] What might the digitisation of this particular photographic archive mean in a time when access to images is increasingly mediated by services such as Google image search? What role can and should an institution such as the Jorn Museum play? The work was to take the form of a research period to become familiar with the particularities of the archive, to be followed by a small installation which eventually would be part of a collection of "hands-on" exhibitions and workshops exploring Jorn's varied techniques. Finally, a small symposium would be held with researchers and open to any interested public.

![](originals/P4274401.JPG)

When we first arrived in Silkeborg to begin work, we were shown to the archive: seven heavy filing cabinets each with four drawers, temporarily moved into in the main exhibition space. We were informed that the left to right order of the cabinets had been inverted as a result of the move up from the museum's basement depot. Placed behind a short black cable boundary, the cabinets had been staged as a kind of simulated workplace complete with an adjacent table and lamp. A number of the photographs had been reproduced and lay strewn about in a pile on the floor with a scenographic flourish vaguely suggestive of a crime scene.[^jornrage] We were given extension cables and additional lighting and got to work, the imagined workspace becoming actual as we unpacked a scanner, plugged in our laptops and began to take notes and photographs.

[^jornrage]: Perhaps the depicted scene was that of Jorn and Franceschi breaking their collaboration and Jorn changing the locks to the archive.

![](originals/P4274355.JPG)

In the cabinet, each image appears in several copies, first as photographic negative, then on a contact sheet (itself a document often marked upon with grease pencil), and finally in multiple reproductions at varying sizes. In this way Jorn and others were free to work from the archive, cutting out copies of images for use in (possible) spreads for publications. This literal form of "cut and paste" and the function of the "copyability" of the photographic negative was a useful reminder of the many physical and manual gestures on which digital tools and workflows have been (metaphorically) based. As Jorn and Arnaud wanted to bring tongues back to "la langue", we started to wonder what it might mean to give something of the "finger" back to the digital.


Green mamba
----------------
A classroom of children sit obediently on a cluster of low stools in a small darkened room. They've been brought by their teachers to the art center in Silkeborg to tour the temporary summer exhibition "Dr. Jorn's Institute of Artistic Disturbance".[^drjorn] In the morning they rolled marbles soaked in ink across paper in a hands-on exploration of some of Jorn's later painting techniques embracing chance. Now, after a lunch of chocolate milk and cheese sandwiches, and a brief run around the triolectic football ring outdoors[^triolecticfootball], the kids have been brought to the relaxing atmosphere of this darkened theater space. On the screen, initially black, suddenly appears a bright black and white image showing a detail of face carved in stone. After a few moments the stationary image is disturbed by what appears to be a worm of color, entering from the bottom edge of the image. The worm moves slowly, leaving a zig zag trace of bright semi-transparent red pixels apparently following the edges of the face, then jumping to an adjacent edge and beginning its trace again in bright green.  The worm works slowly and methodically crawling like a caterpillar along the edge, turning to follow a curve or flourish of the stone, from the bottom of the image to the top. A sense of anticipation builds among the children as the worm climbs higher and higher in the image until suddenly, upon reaching the top, the image flicks off leaving only the multicolored tracings as abstract scribbles against a black background. The children erupt in a spontaneous collective "oooh!" as if they have witnessed a display of fireworks.[^contours]

[^triolecticfootball]: For more on Jorn's triolectric football see: [http://www.smk.dk/en/visit-the-museum/exhibitions/asger-jorn-restless-rebel/about-the-exhibition/try-a-game-of-triolectic-football/](http://www.smk.dk/en/visit-the-museum/exhibitions/asger-jorn-restless-rebel/about-the-exhibition/try-a-game-of-triolectic-football/)

[^contours]: For an online version of this installation see: [http://sicv.activearchives.org/jorn/](http://sicv.activearchives.org/jorn/). A video recording of the reported scene can be found at [https://vimeo.com/152736502](https://vimeo.com/152736502).

[^drjorn]: See [http://www.museumjorn.dk/en/exhibitions/current-exhibitions/dr.-jorns-institute-for-artictic-disturbances/](http://www.museumjorn.dk/en/exhibitions/current-exhibitions/dr.-jorns-institute-for-artictic-disturbances/).

![Image from installation at The Photographers Gallery, April 2016](originals/P1017938.jpg)

We feed a photo of a similar Contour tracing from a subsequent installation[^photographersgallery] into Tensorflow, an open source library for Machine Learning developed by Google. The software comes with a demo program that labels images based on "training data" from another project called ImageNet[^tensorflow]. The output of the program contains the labels (or class names) and their respective "scores" representing the probability of the image to belong to a certain "class":

> green mamba (score = 0.43074)  
vine snake (score = 0.18073)  
shoji (score = 0.02248)  
shower curtain (score = 0.01834)  
African chameleon, Chamaeleo chamaeleon (score = 0.01708)

[^tensorflow]: TensorFlow [https://www.tensorflow.org/](https://www.tensorflow.org/) and ImageNet [http://image-net.org/](http://image-net.org/)

[^photographersgallery]: See [http://thephotographersgallery.org.uk/contours](http://thephotographersgallery.org.uk/contours).

Bag of (visual) words
-----------------------------------------------

"Holy shit, it's working"... Nomi exhaled and instinctively fanned her hands and bowed her head gravely to the monitor in a quasi-sincere gesture of adoration to the algorithm. Just an hour earlier, she had sat exasperated, slumped back in her chair at the computer in the corner of her bedroom. She had been working non-stop during the weekend trying to get her code to work and she'd hit a major snag. The textbook in front of her lay open to chapter 7, "Searching Images", about building a simple image-based search engine.

> Content-based image retrieval (CBIR) deals with the problem of
> retrieving visually similar images from a (large) database of images.
> This can be images with similar color, similar textures or similar
> objects or scenes, basically any information contained in the images
> themselves.
>
> For high-level queries, like finding similar objects, it is not
> feasible to do a full comparison (for example using feature matching)
> between a query image and all images in the database. It would simply
> take too much time to return any results if the database is large. In
> the last couple of years, researchers have successfully introduced
> techniques from the world of text mining for CBIR problems making it
> possible to search millions of images for similar content. The most
> common weighting is tf-idf weighting (term frequency - inverse
> document frequency) But what form exactly does this "information"
> take? [^PCV]

Working with "Computer Vision" techniques was a relatively new area of interest for Nomi. While she had studied some of the techniques back in University during her Computer Science studies, that was two decades ago when the application of such techniques was still the relatively speculative field of robotics. Today, however, it was the masses of smart phone users, rather than the hydraulics of robots, that mobilized these artificial eyes first in the form of face-detection routines automatically adjusting cameras exposure settings and later in the sophisticated tagging and analyses performed by social media platforms once these images were "shared". The book in front was one of those "animal books" from the technical publisher O'Reilly.[^animalbooks] On the cover was an old-style black and white depiction of a "bullhead catfish", that according to the book's colophon was a "bottom-feeder" that due to its high reproductive rate and destructive effect on aquatic ecosystems was considered a curse for fisheries[^PCVcolophon]. Nomi had wondered darkly if this was meant as a wry statement on the implications of computer vision techniques culturally. But these concerns were now far from her mind as she struggled with her code.

[^animalbooks]: The "Animal Menagerie" on the technical publisher O'Reilly's website provides an index of all their technical books ordered by color (in PMS color code), title, and animal used. See [http://www.oreilly.com/animals.html](http://www.oreilly.com/animals.html).

She was trying to match image fragments from a video where images had been projected in a slide show back to the original digital images. Initially she had thought to do the work "by eye", but after a few minutes of flipping through just part of the 300 possible images searching for a match, she realized that the task demanded more visual patience than she could endure. So she was hoping "visual bag of words" might do the trick.

> To apply text mining techniques to images, we first need to create the
> visual equivalent of words. This is usually done using local
> descriptors like the SIFT descriptor in Section 2.2. The idea is to
> quantize the descriptor space into a number of typical examples and
> assign each descriptor in the image to one of those examples. These
> typical examples are determined by analyzing a training set of images
> and can be considered as visual words and the set of all words is then
> a visual vocabulary (sometimes called a visual codebook). This
> vocabulary can be created specifically for a given problem or type of
> images or just try to represent visual content in general.[^PCV]

She had gone through all the steps dutifully. First she ran the SIFT program to detect and dump a "description" of all the "interesting" features of some 300 images. Next she had run a "training step" to build the "visual vocabulary" based on all of the features from all of the images. This step turned out to be quite "computationally expensive", running the code initially on her laptop had led to the computer struggling for half an hour or so before dumping out a dreaded "segmentation fault". So she had moved into her bedroom where a desktop computer with more memory and faster processors could tear into it. After another half hour or so of computation, the algorithm had successfully dumped out the resulting "vocabulary" in the form of a file. She had then re-processed the same 300 images, creating an "index" by recording the "profile" the feature descriptions of each image made when "projected" back onto the statistical average of the vocabulary. Finally she had attempted to "search" the index. In her case, the search image was a still from a video in which a fragment of one of the original images appeared in the background. Her hope was that by searching the index, the original image would come back as a "hit". She ran the video frame through the same process, comparing its feature "profile" to all those in the index and returning a list of hits ordered by similarity. But it didn't seem to be working at all. While the code all ran without errors, the results seemed random and crucially didn't give her back the one match she was looking for.

This wasn't her first attempt at understanding Chapter 7. The book was heavily dog-eared and marked with penciled notes in the margins referring back to pages where concepts had been earlier introduced. The process was initially overwhelming as it seemed a case of infinite regress: each technique seemed to itself lead to others each with their own underlying assumptions and subordinate techniques.

Initially she had been quite bothered by all the references to "interest points" ... "interesting in what way, and to whom?!" she had wondered. She had finally found the most satisfying explanation in a video lecture about SIFT where the comparison was made to fitting the pieces of a jigsaw puzzle together. Given that the computer sees just a fragment of the image, the lecturer explained, "interesting" pieces are like those puzzle pieces with patterns (such as corners rather than flat color) that would ideally fit the overall picture in just one place.[^siftlecture]

[^siftlecture]: This description is based on lectures such as that given by Dr. Mubarak Shah in his 2012 lecture on *Interest Point Detection* [https://youtu.be/_qgKQGsuKeQ?t=4m32s](https://youtu.be/_qgKQGsuKeQ?t=4m32s)

Having come to a dead end with visual bag of words, Nomi decides to abandon the technique and focus solely on the SIFT features themselves. Instead of relying on a search index, she decides to apply a "brute-force" matching algorithm to compare the features of one image to each feature of every other image one by one. Such a technique would never work for a collection of thousands of images, but for her small set it should suffice. She starts the algorithm and lies down to rest while the algorithm plods through its comparisons. After 15 minutes or so she checks the log file and sees that the algorithm is working surprisingly well, for a particular video frame, most comparisons yield no matches; in a few cases there are 1-10 matches, and then, "bam!", one obvious match of over a hundred features. Nomi opens the corresponding image and visually confirms that the fragment is contained within the photograph. In the rush of euphoric relief, she forgets all the work she's done to get to the point and can only marvel at what she perceives as the mightiness of the algorithm at work.

[^PCV]: Jan Erik Solem, *Programming Computer Vision* [O'Reilly 2012]. See [http://programmingcomputervision.com/](http://programmingcomputervision.com/).

[^PCVcolophon]: Solem, p. 247.

[^freesoftware]: Free software indicates that the software is not only freely available (in the sense of being free of cost), but only licensed with a Free software license enabling developers to see, make changes to, and redistribute the underlying source code.

![](originals/framestitch01.png)

![](originals/stitch01.png)[^computationalvandalismvideo]

[^computationalvandalismvideo]: These stills are from an application of the technique described used to stitch the frames of a video onto originals photographs. The results can be seen at: [http://sicv.activearchives.org/audio/computationalvandalism.webm](http://sicv.activearchives.org/audio/computationalvandalism.webm)

Computational Vandalism
-----------------------------------------

The fact that Google Image Search (GIS) uses a mixture of textual cues and visual ones in performing an image search is not in and of itself problematic. The problem stems from the fact that the "secret sauce"[^secretsauce] of a product like GIS is by its nature inscrutable, imprecise, and subject to change without notice. The particular recipe of the day may or may not "bake in" an understanding of an image that subordinates pixels to the words detected in adjacent captions, thus reinforcing a sense of the image as simply an illustration of text. The apparent simplicity of the tool furthers a perception of "digital tools" as black boxes which are somehow natural, inscrutable, inevitable.

[^secretsauce]: >So I would argue that Google really does have a better product than the competition -- not because we have more or better ingredients, but because we have better recipes. And we are continuously improving those recipes precisely because we know the competition is only a click away. We can't fall back on economies of scale, or switching costs, or network effects, to isolate us from the competition. The only thing we can do is work as hard as we can to keep our search quality better than that of the other engines.  
Hal Varian, "Our Secret Sauce", Corporate Google Blog posting [2008] From [https://googleblog.blogspot.be/2008/02/our-secret-sauce.html](https://googleblog.blogspot.be/2008/02/our-secret-sauce.html). Retrieved May 2016.

Visual bag of words is just one example of a "layer cake" of methods and techniques that is typical of computer programming in general, and of computer vision in particular. Different methods and algorithms are often pulled out of the programmers "toolbox" and mixed and matched, the output of one strapped onto the input of another, to perform a task. Again, this fact in and of itself is not problematic. The very interchangeability of tools and techniques is an exciting part of how digital tools can lend themselves to promiscuous and constructive (mis)use and novel assemblage. The difficulty, however, lies in understanding or considering how the assumptions and limitations of one layer influence the others and whether the assumptions of one part of the pipeline are still appropriate later on when one's attention has shifted "up the stack" to another part of an application. "Black boxing" as an engineering process is all about being able to "let go" and forget the details of a particular part of a program to shift one's focus to another. The danger is when, in a process of abstraction where the inner details and context of particular routines development are suppressed, one is tempted to start believing the often exaggerated claims that effectively label the box.

Jorn may well have found the idea of creating "language" based on purely visual aspects (albeit filtered through the techniques of mathematics) as profoundly appealing.[^suppression] But whose language is it, and what do the words refer to? Are they the observations of the anthropologist aiming to trace cultural heritage through "symbolic icons", or rather the cryptic mutterings of an obsessive-compulsive jigsaw player viewed through a loupe?

[^suppression]: Jorn repeatedly referred to the Swedish architect Erik Lundberg’s concept of a "language of form" and suggested that the mathematical field of topology might contribute to a "comprehensive codification" of visual art. See, for instance, his 1967 text "Structuralism and Suppression", translated to English by Niels Henriksen in October 141 [2012], pp. 81-85.

Jorn's offering of overpaintings as visual evidence in a new kind of academic discourse, makes the radical proposal to embrace the subjectivity of the image, asserting the image as something open to multiple interpretations. An algorithmic treatment such as a contour tracing can usefully be seen as itself a kind of overpainting, a performative act contingent on the situated application of algorithm to image. Far from producing an essential reduction that "consumes" its subject, the algorithmic overpainting suggests new ways to read and re-read the image.

How can one embrace the multiplicity of meaning in these new forms of algorithmically determined language, in line with Jorn's tactical deployment of the pun in *La Langue verte*? Jorn's response to a perceived reductive view of language evident in Lévi-Strauss's structuralism was to embrace and display the full capacity of language, both visual and textual, to include multiple meanings, speculations, deliberate ambiguities, insults and offense, fiction, lies, insinuations, absurdities and contradiction. What are the equivalents in a visual vocabulary? What alternatives are there to the frequently used statistical reductions of techniques like k-means and principle component analysis and what are the impacts of using such techniques to create indexes to visual material? As the engineering world moves increasingly to ever more obscure and inscrutable methods such as deep learning, there is an increasing need for a sharpness in ensuring that such techniques actually promote an ever more diverse and expansive image reading culture rather than an ever more banal and narrow one distorted by feedback loops of popularity.

![](originals/collage20160227_004714.jpg)

At the end of Harris' reading of *La Langue verte* he describes the spread, occurring late in the book, where a well-known portrait of Albert Einstein, tongue playfully extended to the camera, has been overpainted in pink by Jorn. Opposite this page is a young woman taking part in the May 1968 protests in Paris, standing in the middle of a street defiantly showing her tongue, presumably to the police. Harris notes how the gesture clearly originates from a joyful position of self-empowerment.[^harristongue] In early 2016, the authors created a small installation for the window of the Constant[^vitrine] office in the Saint-Gilles neighborhood of Brussels. For forty days, a camera was set up to record short sequences of video frames, triggered by the presence of a "face" as detectable by a standard "face-detection" algorithm[^cascade]. When a face was detected, a short sequence of frames would be recorded, and the regions of the image detected as "faces" would be replaced by faces similarly detected in an archive of images, including images from the SICV. Many of the detected "faces", both from the street and the archive, would be considered "false positives" -- images not of faces but of car hubcaps, the folds of a puffy jacket, the typography of a book page, or the play of light on the opposite building. A number of monitors were arranged facing the street in a reversal of the traditional surveillance setup, making the recording apparatus and processing pipeline as visible as possible, showing the transformation step by step to its subject. Once the recordings were completed, a short animation loop was produced and displayed on a central monitor. One Saturday evening, the image above was recorded.

[^harristongue]: Harris, p. 130.

While it may be tempting to interpret the gesture as a statement of disapproval of the installation, something else seems to be at play, suggesting a further meaning of the notion of giving the finger to the digital. The installation was not continuously recording but in fact only recorded 15 frames before entering into the processing loop that took at least a full minute to complete. As a result most of the recorded sequences are accidental, often showing passersby unaware of the camera and with the algorithm fleetingly finding faces, often involving false positives. In this recording however, the subjects stand with their faces fully forward, anticipating the operation of the machine and demonstrating a comprehension of the system and its mechanics. The pair fully address the algorithm, and in so doing they perform it. The gesture of the raised middle finger, then, is made from a position of positive subversion, feeding back a message of resistance to the captured gaze of artificial attention.


[^vitrine]: See [http://constantvzw.org/site/The-Scandinavian-Institute-of-Computational-Vandalism.html](http://constantvzw.org/site/The-Scandinavian-Institute-of-Computational-Vandalism.html)

[^cascade]: Based on the Haar-cascade face detection method. See [http://docs.opencv.org/2.4/modules/objdetect/doc/cascade_classification.html](http://docs.opencv.org/2.4/modules/objdetect/doc/cascade_classification.html)

[^activearchives]: See [http://sicv.activearchives.org/](http://sicv.activearchives.org/).

