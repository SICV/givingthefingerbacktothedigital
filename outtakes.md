outtakes

[^pea]: Once upon a time there was a prince who wanted to marry a princess; but she would have to be a real princess. He travelled all over the world to find one, but nowhere could he get what he wanted. There were princesses enough, but it was difficult to find out whether they were real ones. There was always something about them that was not as it should be. So he came home again and was sad, for he would have liked very much to have a real princess.

	One evening a terrible storm came on; there was thunder and lightning, and the rain poured down in torrents. Suddenly a knocking was heard at the city gate, and the old king went to open it.

	It was a princess standing out there in front of the gate. But, good gracious! what a sight the rain and the wind had made her look. The water ran down from her hair and clothes; it ran down into the toes of her shoes and out again at the heels. And yet she said that she was a real princess.

	"Well, we'll soon find that out," thought the old queen. But she said nothing, went into the bed-room, took all the bedding off the bedstead, and laid a pea on the bottom; then she took twenty mattresses and laid them on the pea, and then twenty eider-down beds on top of the mattresses.

	On this the princess had to lie all night. In the morning she was asked how she had slept.

	"Oh, very badly!" said she. "I have scarcely closed my eyes all night. Heaven only knows what was in the bed, but I was lying on something hard, so that I am black and blue all over my body. It's horrible!"

	Now they knew that she was a real princess because she had felt the pea right through the twenty mattresses and the twenty eider-down beds.

	Nobody but a real princess could be as sensitive as that.

	So the prince took her for his wife, for now he knew that he had a real princess; and the pea was put in the museum, where it may still be seen, if no one has stolen it.

	There, that is a true story.

	Hans Christian Andersen, *Fairy Tales of Hans Christian Andersen*, Public Domain; Retrieved from
	[http://www.gutenberg.org/files/27200/27200-h/27200-h.htm#princess](http://www.gutenberg.org/files/27200/27200-h/27200-h.htm#princess)
	May 2016



	Cabinet, binder, box: The SICV photo archive
------------------------








Powers of Ten
----------------

> We begin with a scene 1 meter wide which we view from just 1 meter away. The sleeping man is having a picnic on a Miami golf course. As we back off the clocks start and we accelerate in such a way that after 10 seconds have elapsed we will be exactly 10 times farther away. Now the scene is 10 meters wide and in 10 seconds it will 10 times that wide. The sleeping man is still in the center and will remain there long after he is lost to sight. After the second 10 seconds … the square we see is 100 meters wide, the distance a man can run in 10 seconds. [^p10_intro]

> Finally it begins to appear, that unimaginably dense nucleus of a carbon atom 12 orbiting parts, 6 protons and 6 neutrons. Our galaxy is 36 powers of ten larger than the nucleus of a carbon atom. If the diameter of the atomic nucleus were a unit 1 then the diameter of the galaxy in atomic units would be 10 to the 35th or 1 and 36 zeroes. [^p10_outro]

[^p10_intro]: The opening narration of *Powers of Ten: A Sketch*, https://vimeo.com/99432635

[^p10_outro]: The ending narration of *Powers of Ten: A Sketch*, https://vimeo.com/99432635

The film is a kind of clock; 

The Eames film *is* a kind of media, setting a framerate; the production of the film is thus a part of its content: every 10 seconds will translate into one meter of movement. The distance of each frame of film to the original subject is thus established (and can thus be produced independent of any other frame).

Similar to Erkki's image making is itself a clock, and one for which he sought to every increase it's *temporal* resolution (with perhaps less concern for the spatial resolution of the image).

> Thank you, Rajeev, for posting this film. I first saw it when I was 10, back in 1968. It was shown
on the Penn State University campus in a presentation of experimental films, as part of the Central Pennsylvania Festival of Arts that year. i have remembered it vividly for the past 47 years but long ago forgotten the title, Hence, I had no way of searching for it. It was by chance that I happened to stumble across it while web surfing on my new iPad. What a nice surprise! Very pleased to see that it has lived on and you have preserved it for all to see.

Powers of 10 explores the limits of human comprehension over 40 "powers of 10" from the microscopic to the universal. A fascinating "amking of film" shows an early black and white prototype of the film . In sketch form, on e is made more aware of the efforts & professional trickerhy (craft) involved in making the illusion appear seamless. The notion of a continuous zoom is used to seemingly merge disparate kind of documents: drawing collage, decopuage, photographs, diagrams, typography, satellite photography, microscopy, electron imaging

The use of phowers of 10 emplyts the logarithmic to enable the steady tick of linear time to explore expoentially changing scales. The resulting linearization, itself catpure intfilm form, reinforeces the comforting idea of "captuing" the complextiy of relaity in the stability of a system, a medium, a rexperssion of the eames own sense of desigining rational systems for organizing topics of vast scale & complexity (as seen in their work on historical timelines of american political history or mathematics).


Eames, Smooth spaces and data overload
---------
In May 1953, A course taught by Kepes, Eames, and Nelson at UCLA

[Eames] spoke, repeatedly in the course of his career iand in reference to this course, of a "sfound education<'"k suggesting that education should be about recombining data. Reflecting on this 1953 course in an article also tttled "The Lanaguage of Cision", he argued that the main concern for designers and managers was to reduce "discontinutity" between disciplines in an information age. ...
> To produce this smooth space between disciplines, Eames felt "vision" was the best tool. To teach students to see, however, must come through esposure to an excess of data. For Eames only through information *inundation* could learning commence. He as very specific on this point; the purpose fo the class was to experiment with "how much information *could* be given to a class." Data overload as pedagogical principle.
> This principle was a long-running one. The work of the Eames Office in education was not contained to the courses Charles taught. Carles and Ray Eames were deeply involved in many exhibitions and in developing educational materials for the public. 
 [^Halpern_p101]

> Charles Eames believed that design whoule *amplify* and accelerate the availability of information. In waht, from the vantagte point of contemporary debates over attention deficit disorcers, now appears as anathema, Eames spoke of distractiona nd overstimulation as an education. Rather than worrying about information overload, Eames thought more data offered more "choice<" giving the spectator a freedom to "choose" from and produce his (or her) own patterns and combinations. He would speak throughout his carerre about "connection" and making connections possible was the purpose of good design. (p. 102)

Connection with Claude Shannon:
> Redefining information not as an index of a past or present event but as the *potential for future actions (not what you say but what you *could* say) encouraged engineers to defer concern about particular messages and to refocus on interactions between sources of signals, in a move that mirrors the rutgn in design to complexity, process, and connection. (p. 103)


<p style="color: gray">
Beyond just showing the well established mechanism that google uses many factors to customize search results (personal search history, language settings, geographic/national access), there are concerns raised that are specific to Image search. While the search filters suggest they operate on the pixel content of the image, google's image search interface blurs the line between formal aspects of the image, and more texual semantic language indexing for which Google is best known (and indeed more successful because of). The imprecision of the interface is representative of the kind of systemic ambiguity[^systemicambiguity] employed by many contemporary web platforms that hides details in the name of ease of use, but at the cost of precision for an advanced user getting a better handle on her search result. This hiding reflects not only a design decision, but the economic model of a commercial company where the secrecy, changability, and complexity of their "secret sauce"[^secretsauce] is an explicit part of their business model.
</p>

[^systemicambiguity]: In the sense described in Fuller, Goffey *Evil Media* [MIT Press, 2012] See: [https://monoskop.org/media/text/fuller_goffey_2012_evil_media/#intelb](https://monoskop.org/media/text/fuller_goffey_2012_evil_media/#intelb)

[^secretsauce]: >So I would argue that Google really does have a better product than the competition -- not because we have more or better ingredients, but because we have better recipes. And we are continuously improving those recipes precisely because we know the competition is only a click away. We can't fall back on economies of scale, or switching costs, or network effects, to isolate us from the competition. The only thing we can do is work as hard as we can to keep our search quality better than that of the other engines.  
Hal Varian, "Our Secret Sauce", Corporate Google Blog posting [2008] From [https://googleblog.blogspot.be/2008/02/our-secret-sauce.html](https://googleblog.blogspot.be/2008/02/our-secret-sauce.html) retrieved May 2016

<p style="color: gray">
Google is in the business of erasing boundaries; Over time Google has moved from the scraping of resources made available on the web[^pagerank] to actively digitising and indexing (national) libraries, and increadingly visual material in the form of public and private art collections, applying to this material standardised processes of digitisation and mass indexing. Under the banner of open access, collections are digitized en masse, or to use vocabulary often found in the context of "linked data" they are consumed.[^linkeddata]
</p>

[^pagerank]: Google's initial quality and value as a service was based on efficacy of the PageRank algorithm in exploiting web authors' linking structures. See Brin, Page *The Anatomy of a Large-Scale Hypertextual Web Search Engine* [1998] available at [http://infolab.stanford.edu/~backrub/google.html](http://infolab.stanford.edu/~backrub/google.html)
[^linkeddata]: [https://webofdata.wordpress.com/2010/06/04/linked-data-consumption-where-are-we/](https://webofdata.wordpress.com/2010/06/04/linked-data-consumption-where-are-we/)

* Generality of results
* Non-specificity of sources
While it's great that Google's crawls include popular content 

> Google didn’t pick random collections of content. Instead the company’s venture into the world of literature started by digitizing millions of books from some of the most venerable research libraries — Harvard, the New York Public Library, Stanford, the University of Michigan and Oxford — with plans to expand to libraries around the world. These are highly curated collections of books which have been built by cultural workers over hundreds of years, and with considerable public funding.  As Google devoured these library collections, the company also invisibly absorbed the countless hours of labor that had been expended by the cultural workers who had built those collections – without paying a dime in compensation and with few commitments to any kind of democratic accountability beyond “access.” [^pillage]

[^pillage]: *Appellate Court Ruling For Google Books: Fair Use, or Anti-Democratic Preemption?* [Information Observatory 2015] Retrieved from [http://informationobservatory.info/2015/10/27/google-books-fair-use-or-anti-democratic-preemption/#more-279](http://informationobservatory.info/2015/10/27/google-books-fair-use-or-anti-democratic-preemption/#more-279) May 2016

> *Naming is a serious process.* It has been of crucial concern for many individuals within oppressed groups who struggle for self-recovery, for self-determination. It has been important for black people in the United States. Think of the many African-American slaves who renamed themselves after emancipation or the use of nicknames in traditional folk communities, where such names act to tell something specific about the bearer. Within many folk traditions globally, among the Inuit, the Australian Aborigines, naming is a source of empowerment, an important gesture in the process of creation. A primacy is given to naming as a gesture that deeply shapes and influences the social construction of a self. As in southern African-American folk traditions, a name is perceived as a force that has the power to determine whether or not an individual will be fully self-realized, whether she or he will be able to fulfill their destiny, find their place in the world.[^bellhooks]

[^bellhooks]: bell hooks, *Talking Back: thinking feminist, thinking black* [Boston: South End Press, 1989], p. 166


> *A language, on the contrary, is something in which everyone participates all the time,* and that is why it is constantly open to the influence of all. This key fact is by itself sufficient to explain why a linguistic revolution is impossible. Of all social institutions, a language affords the least scope for such enterprise. It is part and parcel of the life of the whole community, and the community's natural inertia exercises a conservative influence upon it... Ultimately there is a connexion between these two opposing factors: the arbitrary convention which allows free choice, and the passage of time, which fixes that choice. It is because the linguistic sign is arbitrary that it knows no other lay than that of tradition, and because it is founded upon tradition that it can be arbitrary.[^saussure]

Sassure himself seems to speak from opposing factors: one the one hand "democratic" as language is "open to all", yet also celebrating the normativity of speaking from a position of authority (revolution is impossible, long live the arbitrary!) 

[^saussure]: F. de Saussure, *Course in General Linguistics*, trans. Roy Harris [London: Duckworth, 1983], p. 74
[^saussure_p9]: F. de Saussure, *Course in General Linguistics*, trans. Roy Harris [London: Duckworth, 1983], p. 9


> Language at any given time involves an estrablished system and an evolution. At any given time, it is an institution in the present and a product of the past. At first sight, it looks very easy to distinguish between the system and its history, between what it is and what it was. In reality, the connexion between the two is so close that it is hard to separate them. Would matters be simplified if one considered the ontogenesis of linguistic phenomena, beginning with a study of children's language, for example? No. It is quite illusory to believe that where language is concerned the problems of origins is any different from the problem of permanent conditions. There is no way out of the circle.[^saussure_p9]

> The fundamental "error" in psychological and physiological conceptions of temporality, according to this reading of Bergson, is the idea that there is a clear separation between sensory perception and representation (or thought); that we first perceive an external world, store that perception as recollection, and then retrieve it, in an set order with clear differentiation between on the one hand the material and embodied actions on one hand and on the other the cognitive thought processes. Rather as Gilles Deleuze explains, "We do not move from the present to the past, from perception to recollection, but from the past to the present, from recollection to perception." One might say that the mind does not simply respond to and synthesize a series of abstractions gathered from the external sense, which preceded the thought, or the image; rather, both action and thought are coconstituted. The experience of perception for the human being is always, therefore, produced in the lag between the stimulus and its recollection; it is inextricability linked to both recording and recalling. [^halpern_300]

[^halpern_300]: Orit Halpern, "Dreams for Our Perceptual Present: Temporality, Storage, and Interactivity in Cybernetics," in *Configuration* [The Johns Hopkins University Press, 2007], p. 300


Giving the finger (back) to the digital
------------------------------------

What is strange about the arbitrariness of the black box / mixing and matching of techniques is that details of context are seemingly too often thrown aside as a sealed box technique gets reused in another context with seemingly little regard to what the original context of the problem was. In this case, SIFT features, themselves developed in the context of applications like photo stitching are then applied as a descriptive basis for a bag of words index of an image.

* Resistance of a privileging of the index to the document (and in this case the Image)
* cf Teresa Pedersen here?!

Clearly Google is representing images in a multiplicity of ways, some based on textual aspects of the image in its use online, and other based on the formal properties of the image pixels themselves. What's problematic is that these differences are kept obscure, maintained as proprietary "service" secrets, are prone to change based on the shifting demands of a global commercial enterprise, complex in its potential use of usage data of both the querying user and others.

There's something else that links "visual" bag of words to language; more than just a conventience of methods, VBOW emphasizes how all language involves potentially arbitrary symbols (when considered in isolation of their use). Language is a circulating system that both refers to the past and shapes the future.

Language is born from the needs and desires of a particular moment. The "interest" points of SIFT are "interesting" in the way that in the context of images both depicting the same object, the algorithm is able to find corresponding points that match. Thus the points, like words themselves, are rarely interesting in and of themselves but in their functioning in larger assemblages of expression.

speak back to
algorithms to "generate art" ... "forgetting" the sources and de-humanizing the human
(parallel to google's greatest sin is not giving access to images, but to obscuring their origins, destabilizing the archives that collected them in the first place, and rewriting the indexes to further this distortion/displacement).

(Trace the origin of Negro Art magazine as an archive, & possibly the book !!)

If google were a good librarian....

an image as a surface to draw upon (literally) as both a means of REVIEWING / REVALUING the original, and as a basis for creating something new (here could use JORN again!)....

Clicking on the google image result showing the issue of "Negro Digest" and the headline "Black World" led me to [a page on pinterest titled "black culture <3"](https://www.pinterest.com/airelstewart/black-culture-3/). The image in question is findable on the page by scrolling, but as I scroll, a semi-opaque black background shifts over the list with the message that "There's more to see on Pinterest" and imploring me to login. Following the link further reveals that the image had been posted and then pinned from a [tumblr blog specialising in magazine covers](http://newmanology.tumblr.com/).

In reading computer vision code tutorials, it's common to see texts like, the such and such parameter controls the sigma value, the square root of two is often a good value.,





> We ought then to regard the present state of the universe as the effect of its anterior state and the cause of the one which is to follow. Given for one instant an intelligence which could comprehend all the forces by which nature is animated and the respective situation of the beings who compose it - an intelligence sufficiently vast to submit this data to analysis - it would embrace in the same formula the movements of the greatest bodies of the universe and those of the lightest atom; for it, nothing would be uncertain and the future, as the past, would be present in its eyes.[^laplace]

[^laplace]: http://www-history.mcs.st-and.ac.uk/Extras/Laplace_Probabilities.html

> LoG is an acronym standing for Laplacian of Gaussian, DoG is an acronym standing for difference of Gaussians (DoG is an approximation of LoG), and DoH is an acronym standing for determinant of the Hessian. These scale-invariant interest points are all extracted by detecting scale-space extrema of scale-normalized differential expressions, i.e., points in scale-space where the corresponding scale-normalized differential expressions assume local extrema with respect to both space and scale.[^log]

[^log]: [https://en.wikipedia.org/wiki/Corner_detection#Laplacian_of_Gaussian.2C_differences_of_Gaussians_and_determinant_of_the_Hessian_scale-space_interest_points](https://en.wikipedia.org/wiki/Corner_detection#Laplacian_of_Gaussian.2C_differences_of_Gaussians_and_determinant_of_the_Hessian_scale-space_interest_points)

TODO: Document the application of SIFT to the video stitch of Matthew
* Establish SIFT as a stitching algorithm and the idea that the descriptor is not so much important per se, but in terms of it usefulness to match corresponding points across images
* So "Interest" in the sense of a jigsaw puzzle players interests.
* Highlight the importance of cutting the algorithm off, the matcher happily finds matches -- often the "successful" use of SIFT and other CV tools lies in the the importance is tuning the parameters and the cutoffs to balance signficant results from "errors". The problematics of this are double: (1) It hides the physical human hand/eye work that guides the tools,
(2) it obscures the algorithmic which, in its errors, reveals the techniques own perculiarities (and possibilities).
(Interesting in the SIFTing stones example is that the "original" object in this case is not the stone per se but the photograph of it as the match occurs between the photograph and the re-recording image where that same photograph was projected behind the speaker). The algorithms resistance to rotation, scale, and skew now come into play not in the movement of the image's subject, but in it's own rotation through projection, and the specific material properties of the wall onto which the image is reflected.


The installation took the form of computer and scanner along with the filing cabinets situated in one room and an adjacent projection space with informal seating. Visitors coudld scan reproductions of the photographs of the archive, pages from books, or possibly their own hands or phones. The scanned image would then be uploaded to an online archive and an algorithmic tracing of the image's "contours" would be subsequently displayed in the projection room. When the scanner was not in use, the projection would cycle through the past collection of scans including an initial selection of images from the archive. The specific algorithm used was the "contour detection" -- a standard part of the OpenCV library. In addition to tracing the edges detected between areas of contrast in an image, the algorithm separates these edges into a series of discrete segments attempting to find the longest contiguous edges possible. The contours are then redrawn in a transparent stroke on top of the image, with each segment receiving a unique random color.



Stitching stones
--------------------------



A more recent and highly successful computer vision technique is the SIFT feature. SIFT, short for scale invariant feature transform, is a patented technique developed in 200? for detecting "interest points" also known as "local features" (local meaning in this case specific to a particular portion of an image rather than a "global" feature involving the entire image). The technique has in fact 3 dinstinct parts: (1) a detector, (2) a descriptor, and (3) methods for comparing (matching). Given an image, the detector responds with specific (and possibly overlapping) circular cutouts of the image (of various sizes) that are considered "interesting". The descriptor is then a technique that given a particular circular cutout produces a particular "description" which in the base of SIFT is a list of 128 numbers. When first exposed to computer vision techniques, terms like "interesting" tend to slip by with little discussion of what is actually meant (such as interesting to whom, in what way?), and the notion of a "description" being in fact a long list numbers is naturalized to the point of invisibility.

In fact, "interest" in the case of SIFT is perhaps best described metaphorically in terms of the player of a jigsaw puzzle. Interesting pieces are ones where distinctive marks occur such as the corners of a building, rather than in areas of flat unchanging colors (like a blue sky). The "description" on the other hand remains rather difficult to grasp intuitively. SIFT descriptors are a kind of "histogram of gradients" (or HOG for short) which basically represent a collection of "visual flows" or measurements of the relative amount of dark to light change in patches of the selected area in each of 8 compass directions. Futhermore, the entire system of flow measurements is rotated in the direction of maximal overal flow. 

In any case, ultimately the importance of the SIFT descriptor is not so much in what it says about an individual point, but as a means or handle of making comparisons between images. SIFT features are the key technique in use in stitching photographs together, such as the many individual frames taken by vehicles Google uses to then create its "street view" panaromas.

![](resize/framestitch01.png.800x.jpg)

Another probe we performed after Silkeborg involved using computer vision techniques to project the frames of a 5 minutes of a video of Matthew Fuller speaking at the closing symposium back on to the original stone images. This was possible because Matthew was filmed standing in front of the projection wall. Using a technique called SIFT, or scale-invariant feature transform, the frames of the video were one by one analyzed to detect "interest points" which could then be compared to interest points detected in the corresponding original photographs (the image which was being projected and traced at that moment on the wall behind him). What was interesting about this application was that the photographs of the stones in effect stabilized the video, which has in fact shot hand held from the back of the room with varying levels of zoom. Thus the shaky movements of the shifting frames were in effect reversed through matching corrsponding points from video of the projected (digitized) photograph of a stone onto the digital photograph. Thus a fourth generation image maps to a third generation digital copy. The mapping is imperfect however, at times glitching and stetching the video frames in unexpected ways, or in the periods of black between the projections, the image falls away as no matching points can be detected (at least points need to be found).

![](resize/stitch01.png.800x.jpg)

Possibly the most challenging work was in locating the original images that were displayed during the selected portion of the recording. Visually it was possible to note that 13 different images were displayed in the 5 minutes of the recording. However, figuring out which images were being projected was beyond the scope of our individual patience to reverse engineer, especially considering (1) there were over 200 images in a random loop at the time, and (2) the video often would show only a small portion of the photograph. In the end, we used a "brute force" method where 13 representative video frames were compared one by with each of the 200 candiate photographs and the number of matching interest points recorded. The calculate took some 12 hours to complete but the results were suprisingly accurate, for each of the 13 frames, there was always a single photograph that produce a large (> 3) number of matching features and in each of these cases, these were the corresponding images. 


![](originals/vvlayercake.png)




> In a Situationist polemic against the activities of the Lettrist Maurice Lemaitre, Jorn states that, “Nothing is worse than stupidity combined with a never failing memory.” Although we can have doubts about the latter quality on occasion, this pair of assets is among the range of exciting features that computing offers. Computers are stupid in the sense that they do exactly as they are told, so the art of programming is partially in making such instructions specific. Their capacity for memory, as this function of stupefied perfect recall, is what makes them so effective for archiving, and indeed so disturbing as an agent of social control. Working with such a system, in all of its many layerings allows, however, for other kinds of compositional dynamic to be drawn out, to be elicited by the dogged nature of the electronic computing machine.

> Computational vandalism is a mode of working with this and other qualities of computing: the capacity for repetition, speed, interpretation by combination, the layering of operations and so on. Here, mathematical, procedural and logical forms combine and differentiate their propensities in relation to different types of operation and their coming into composition with others. In this sense, computational vandalism works with the aesthetic, social, material and imaginal forces that are gathered as compositional terms within computing. The art here is in discerning and making subtle, blatant or seductive some of these qualities, drawing out in turn their capacity to work on things.[^computationalvandalism]

[^computationalvandalism]: Matthew Fuller, http://sicv.activearchives.org/w/Computational_Vandalism


	How to start describing an image, run feature detector (Chapter 2). Too many features to handle? Use k-means to reduce them to statistical clusters. Resulting clusters are still not specific enough? Try "tf-idf" to weigh then in the style of text documents.
