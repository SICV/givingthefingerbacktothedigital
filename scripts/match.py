from __future__ import print_function
import numpy as np
import cv2
from matplotlib import pyplot as plt
from argparse import ArgumentParser


p = ArgumentParser()
p.add_argument("in1")
p.add_argument("in2")
p.add_argument("--output", default="match.svg")
args = p.parse_args()

# http://docs.opencv.org/2.4.9/

img1 = cv2.imread(args.in1,0)          # queryImage
img2 = cv2.imread(args.in2,0) # trainImage

# Initiate detector
orb = cv2.ORB()

# find the keypoints and descriptors
kp1, des1 = orb.detectAndCompute(img1,None)
kp2, des2 = orb.detectAndCompute(img2,None)

# create BFMatcher object
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

# Match descriptors.
matches = bf.match(des1,des2)

# Sort them in the order of their distance.
matches = sorted(matches, key = lambda x:x.distance)

# for x in matches[:10]:
# 	print x, x.distance, dir(x)
	# distance, imgIdx, queryIdx, trainIdx

# The result of matches = bf.match(des1,des2) line is a list of DMatch objects. This DMatch object has following attributes:

#         DMatch.distance - Distance between descriptors. The lower, the better it is.
#         DMatch.trainIdx - Index of the descriptor in train descriptors
#         DMatch.queryIdx - Index of the descriptor in query descriptors
#         DMatch.imgIdx - Index of the train image.


# Draw first 10 matches.
# img3 = cv2.drawMatches(img1,kp1,img2,kp2,matches[:10], flags=2)

from random import randint

with open (args.output, "w") as f:

	print ("""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
""", file=f)


	print ("""<style>
line.match {
	stroke-width: 5;
}
</style>
""", file=f)

	dx = img1.shape[1] + 50

	print ("""<g>
    <image
       y="0"
       x="0"
       xlink:href="{0}"
       height="{1}"
       width="{2}" />
</g>""".format(args.in1, img1.shape[0], img1.shape[1]), file=f)
	print ("""<g>
    <image
       y="0"
       x="{3}"
       xlink:href="{0}"
       height="{1}"
       width="{2}" />
</g>""".format(args.in2, img2.shape[0], img2.shape[1], dx), file=f)


	# draw matches
	print ("""<g>""", file=f)
	for m in matches[:20]:
		a, b = kp1[m.queryIdx], kp2[m.trainIdx]
		print (m.distance)
		rcolor = "rgb({0}, {1}, {2})".format(randint(0, 255), randint(0, 255), randint(0, 255))
		print ("""<line x1="{0}" y1="{1}" x2="{2}" y2="{3}" stroke="{4}" class="match" />""".format(a.pt[0], a.pt[1], dx+b.pt[0], b.pt[1], rcolor), file=f)

	print ("""</g>""", file=f)

	print ("""</svg>""", file=f)

# plt.imshow(img3),plt.show()

