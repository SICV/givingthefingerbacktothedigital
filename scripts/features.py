import cv2
import numpy as np
from argparse import ArgumentParser


p = ArgumentParser()
p.add_argument("input")
p.add_argument("--output", default="features.jpg")
args = p.parse_args()

img = cv2.imread(args.input)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# detector = cv2.SIFT()
detector = cv2.ORB()
kp = detector.detect(gray, None)

img = cv2.drawKeypoints(gray, kp, color=(0,255,0), flags=4)
cv2.imwrite(args.output, img)