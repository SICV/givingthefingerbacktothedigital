# find all .md files in the directory
mdsrc=$(shell ls *.md)
# map *.mp => *.html for mdsrc
html_from_md=$(mdsrc:%.md=%.html)

originals=$(shell ls originals/*)
resize800=$(originals:originals/%=resize/%.800x.jpg)
# resize1024=$(originals:originals/%=resize/%.1024x.jpg)
# resize640=$(originals:originals/%=resize/%.640x.jpg)
# features=$(resize1024:resize/%.1024x.jpg=features/%.1024x.jpg)

all: $(html_from_md) $(resize800) visual_vocabularies.epub visual_vocabularies.pdf visual_vocabularies.odx

# Implicit rule to know how to make .html from .md
%.html: %.md
	pandoc --from markdown \
		--to html \
		--standalone \
		--template template.html \
		--css styles.css \
		$< -o $@

%.epub: %.md
	pandoc --from markdown \
		--to epub \
		--standalone \
		--css styles.css \
		$< -o $@

%.pdf: %.md
	pandoc --from markdown \
		--to latex \
		--standalone \
		--css styles.css \
		$< -o $@

%.odx: %.md
	pandoc --from markdown \
		--standalone \
		--css styles.css \
		$< -o $@

# %.pdf: %.md
# 	pandoc --from markdown \
# 		--to latex \
# 		--standalone \
# 		$< -o $@

resize/%.1024x.jpg: originals/%
	mkdir -p resize
	convert $< -resize 1024x $@

resize/%.800x.jpg: originals/%
	mkdir -p resize
	convert $< -resize 800x $@

resize/%.640x.jpg: originals/%
	mkdir -p resize
	convert $< -resize 640x $@

features/%.jpg: resize/%.jpg
	mkdir -p features
	python features.py $< --output $@

# special rule for debugging variables
print-%:
	@echo '$*=$($*)'
